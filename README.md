![Dilbert random](http://dilbert.com/dyn/str_strip/000000000/00000000/0000000/000000/00000/2000/300/2318/2318.strip.gif)

-- [dilbert.com](http://dilbert.com/)

# Kick It!

An app used to (pseudo) randomly create table soccer teams.

This is the master project containing the following modules:

## kick-it-android

The Android app.

Find it [here](https://play.google.com/store/apps/details?id=nl.bigo.kickit.android&hl=en)
in Google Play.

## kick-it-model

The module that holds the shared API/model between client and server.

## kick-it-wss

The web socket server that keeps track of the game state, and all connected (web socket) clients.