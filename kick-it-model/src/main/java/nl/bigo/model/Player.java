package nl.bigo.model;

/**
 * A player in a table soccer match.
 */
public class Player {

  public final String email;
  private Team team = Team.UNKNOWN;

  public Player(String email) {
    if(email == null) {
      throw new IllegalArgumentException("'email' cannot be null");
    }
    this.email = email.toLowerCase();
  }

  public Team getTeam() {
    return team;
  }

  public void setTeam(Team team) {
    this.team = team;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Player player = (Player) o;

    return email.equals(player.email);
  }

  @Override
  public int hashCode() {
    return email.hashCode();
  }

  @Override
  public String toString() {
    return email;
  }
}
