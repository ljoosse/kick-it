package nl.bigo.kickit.wss;

import com.google.gson.Gson;
import nl.bigo.model.Game;
import nl.bigo.model.Player;
import nl.bigo.model.Team;
import nl.bigo.model.WSEvent;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.*;

/**
 * The web socket server that keeps track of the game state, and
 * all connected (web socket) clients.
 */
public class WSService extends WebSocketServer {

  private static final Gson GSON = new Gson();
  private static final Logger log = LoggerFactory.getLogger(WSService.class);

  private static final int COUNT_DOWN_SECONDS = 60;
  private static final int TOTAL_PLAYERS = 4;
  private static final int END_SCORE = 10;

  // The game state.
  private Game game = new Game(TOTAL_PLAYERS, END_SCORE);

  // A flag indicating if the count-down time is running.
  private boolean timerRunning = false;

  // A flag used to interrupt the count-down thread.
  private boolean timerStopped = false;

  public WSService(int port) throws UnknownHostException {
    super(new InetSocketAddress(port));
  }

  @Override
  public void onOpen(WebSocket client, ClientHandshake clientHandshake) {
    log.info("onOpen, webSocket=" + client.getRemoteSocketAddress());
    sendTo(new WSEvent(WSEvent.Type.GAME, game), client);
  }

  @Override
  public void onClose(WebSocket webSocket, int i, String s, boolean b) {
    log.info("onClose, webSocket=" + webSocket.getRemoteSocketAddress());
  }

  /**
   * Called when one of the connected clients sends this server a message.
   *
   * @param client
   *     the web socket client that sent the message.
   * @param eventJson
   *     the WSEvent, as a JSON string.
   */
  @Override
  public void onMessage(WebSocket client, String eventJson) {

    WSEvent event = GSON.fromJson(eventJson, WSEvent.class);

    log.info("eventJson=" + eventJson);

    boolean scored = false;

    synchronized (game) {
      try {
        switch (event.type) {
          case RED_SCORE:
            game.incrementRedScore();
            sendToAll(new WSEvent(WSEvent.Type.GAME, game));
            scored = true;
            break;
          case BLUE_SCORE:
            game.incrementBlueScore();
            sendToAll(new WSEvent(WSEvent.Type.GAME, game));
            scored = true;
            break;
          case RED_SCORE_UNDO:
            game.incrementRedScoreUndo();
            timerStopped = true;
            sendToAll(new WSEvent(WSEvent.Type.GAME, game));
            break;
          case BLUE_SCORE_UNDO:
            game.incrementBlueScoreUndo();
            timerStopped = true;
            sendToAll(new WSEvent(WSEvent.Type.GAME, game));
            break;
          case TOGGLE:
            Player player = event.getTypeInstance(Player.class);
            game.toggle(player);
            sendToAll(new WSEvent(WSEvent.Type.GAME, game));

            if (!game.isStarted() && game.hasSufficientPlayers() && !timerRunning) {
              // A player joined causing there to be enough players for a game. Start the timer.
              startTimer();
            }
            else if (timerRunning && !game.hasSufficientPlayers()) {
              // There were enough players, but one of them left causing there to be to few players for a game.
              timerStopped = true;
            }

            break;
          case GAME:
            sendTo(new WSEvent(WSEvent.Type.GAME, game), client);
            break;
          default:
            log.warn("unknown event type: " + event.type);
            break;
        }
      }
      catch (Exception e) {
        sendTo(new WSEvent(WSEvent.Type.REJECT, e.getMessage()), client);
      }

      if(scored && game.hasWinner()) {
        startTimer();
      }
    }
  }

  /**
   * When the game state reached the minimum amount of players needed
   * to play a game, a timer is started (and every second all connected
   * clients are informed) and when this timer reaches 0, the teams are
   * divided and the game will start.
   *
   * Note that whenever the minimum amount of players is reached and one
   * of them leaves, the timer is interrupted and the game will (obviously)
   * not start.
   */
  private void startTimer() {

    timerRunning = true;
    timerStopped = false;

    new Thread(new Runnable() {
      @Override
      public void run() {

        int seconds = COUNT_DOWN_SECONDS;

        while (seconds >= 0 && !timerStopped) {

          seconds--;

          try {
            Thread.sleep(1000L);
          }
          catch (InterruptedException e) {
            // Safely ignored.
          }

          // Inform all connected clients of another count-down.
          sendToAll(new WSEvent(WSEvent.Type.COUNT_DOWN, seconds));
        }

        sendToAll(new WSEvent(WSEvent.Type.COUNT_DOWN, -1));
        timerRunning = false;

        if (!timerStopped) {
          // Only start the game is the timer was not interrupted before reaching 0.

          synchronized (game) {
            if(game.getScoreBlue() == 0 && game.getScoreRed() == 0) {
              game.makeTeams();
            }
            else {
              game.clear();
            }
            sendToAll(new WSEvent(WSEvent.Type.GAME, game));
          }
        }
      }
    }).start();
  }

  @Override
  public void onError(WebSocket webSocket, Exception e) {
    log.info("onError, error=" + e.getMessage());
  }

  private void sendTo(WSEvent event, WebSocket client) {

    log.info("sending to 1 connection -> " + event);

    client.send(GSON.toJson(event));
  }

  /**
   * Send an event to all connected clients.
   *
   * @param event
   *     the event to send to all clients.
   */
  private void sendToAll(WSEvent event) {

    Collection<WebSocket> connections = connections();

    log.info("sending to all connections (" + connections.size() + ") -> " + event);

    synchronized (connections) {
      for (WebSocket connection : connections) {
        try {
          connection.send(GSON.toJson(event));
        }
        catch (Exception e) {
          log.info("something went wrong trying to send a message: " + e.getMessage());
        }
      }
    }
  }

  /**
   * The entry point of this server.
   *
   * @param args
   *     not used.
   *
   * @throws java.net.UnknownHostException
   *     when a server could not be started from the machine.
   */
  public static void main(String[] args) throws UnknownHostException {

    WSService service = new WSService(8887);
    service.start();

    log.info("WSService started on port: " + service.getPort());

    BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

    while (true) {

      try {
        // Read command line input.
        String input = in.readLine().toLowerCase().trim();

        if (!input.isEmpty()) {

          if (input.equals("clear")) {
            // Reset the game.
            service.game.clear();
            service.sendToAll(new WSEvent(WSEvent.Type.GAME, service.game));
          }
          else if (input.matches("kick\\s+[1-4]")) {
            // Kick a certain player from the game.
            String[] tokens = input.split("\\s++");
            Integer number = Integer.valueOf(tokens[1]);
            service.game.getPlayers().remove(number - 1);
            service.sendToAll(new WSEvent(WSEvent.Type.GAME, service.game));
          }
          else if (input.equals("exit")) {
            // Stop the service (CTRL+C also works).
            service.stop();
            break;
          }
          else {
            // Something unknown.
            log.info("unknown command: " + input);
          }
        }
      }
      catch (Exception e) {
        e.printStackTrace();
      }
    }
  }
}