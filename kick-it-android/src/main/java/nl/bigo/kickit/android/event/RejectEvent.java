package nl.bigo.kickit.android.event;

/**
 * An event that is fired on the event bus when a web-socket
 * sent an event to the server but the server rejected this.
 */
public class RejectEvent {

  /**
   * The message from the server.
   */
  public final String message;

  public RejectEvent(String message) {
    this.message = message;
  }

  @Override
  public String toString() {
    return "RejectEvent{" +
        "message='" + message + '\'' +
        '}';
  }
}
