package nl.bigo.kickit.android;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import nl.bigo.model.Player;
import nl.bigo.model.Team;

import java.util.List;

/**
 * A simply lit view adapter that binds a player (nothing more than
 * an e-mail address) to the entry of a list view.
 */
public class PlayerAdapter extends ArrayAdapter<Player> {

  public PlayerAdapter(Context context) {
    super(context, R.layout.player_item);
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {

    View view = convertView;

    final Context context = super.getContext();
    final Resources res = context.getResources();

    if (view == null) {
      LayoutInflater inflater = LayoutInflater.from(context);
      view = inflater.inflate(R.layout.player_item, parent, false);
    }

    ViewHolder holder = (ViewHolder) view.getTag();

    if (holder == null) {
      holder = new ViewHolder(view);
      view.setTag(holder);
    }

    final Player player = this.getItem(position);

    int colorId = R.color.light_gray;

    if(player.getTeam() == Team.RED) {
      colorId = R.color.red;
    }
    else if(player.getTeam() == Team.BLUE) {
      colorId = R.color.blue;
    }

    holder.player.setBackgroundColor(res.getColor(colorId));
    holder.player.setText(player.email);

    return view;
  }

  public void setData(List<Player> data) {

    clear();

    if (data != null) {
      for (Player player : data) {
        add(player);
      }
    }

    notifyDataSetChanged();
  }

  private static class ViewHolder {

    TextView player;

    ViewHolder(View view) {
      this.player = (TextView) view.findViewById(R.id.txt_player);
    }
  }
}