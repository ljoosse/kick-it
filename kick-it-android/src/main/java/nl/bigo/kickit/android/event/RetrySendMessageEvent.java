package nl.bigo.kickit.android.event;

import nl.bigo.model.WSEvent;

/**
 * An event that is fired on the event bus when an event
 * failed to be sent to the server.
 */
public class RetrySendMessageEvent {

  /**
   * The event that failed to be sent to the server.
   */
  public final WSEvent event;

  public RetrySendMessageEvent(WSEvent event) {
    this.event = event;
  }
}
