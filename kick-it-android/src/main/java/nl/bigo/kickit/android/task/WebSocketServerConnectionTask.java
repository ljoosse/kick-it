package nl.bigo.kickit.android.task;

import android.os.AsyncTask;
import android.util.Log;
import nl.bigo.kickit.android.R;
import nl.bigo.kickit.android.WSClient;
import nl.bigo.kickit.android.event.BusProvider;
import nl.bigo.kickit.android.event.ToastEvent;
import nl.bigo.kickit.android.event.WebSocketClientConnectedEvent;
import nl.bigo.model.WSEvent;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * An async task that connects a client to the web-socket server.
 */
public class WebSocketServerConnectionTask extends AsyncTask<Void, Void, Boolean> {

  private static final String TAG = WebSocketServerConnectionTask.class.getName();

  private static final String WS_HOST = "big-o.nl";
  private static final int WS_PORT = 8887;
  private static final String WS_SERVER = String.format("ws://%s:%s", WS_HOST, WS_PORT);

  private final Object onSuccess;
  private WSClient client;

  public WebSocketServerConnectionTask() {
    this(null);
  }

  public WebSocketServerConnectionTask(Object onSuccess) {
    this.onSuccess = onSuccess;
  }

  @Override
  protected Boolean doInBackground(Void... params) {

    try {
      client = new WSClient(new URI(WS_SERVER));
      client.connect();
      Log.d(TAG, "connected!");
      return true;
    }
    catch (URISyntaxException e) {
      Log.e(TAG, "could not connect to " + WS_SERVER, e);
      return false;
    }
  }

  @Override
  protected void onPostExecute(Boolean success) {

    Log.d(TAG, "onPostExecute, success=" + success);

    if(success) {

      BusProvider.getBus().post(new WebSocketClientConnectedEvent(client));

      if(onSuccess != null) {
        BusProvider.getBus().post(onSuccess);
      }
    }
    else {
      BusProvider.getBus().post(new ToastEvent(R.string.server_down));
    }
  }
}
