package nl.bigo.kickit.android.event;

import nl.bigo.model.Game;

/**
 * An event that is fired on the event bus with the current game state.
 */
public class GameEvent {

  /**
   * The current game state.
   */
  public final Game game;

  public GameEvent(Game game) {
    this.game = game;
  }
}
